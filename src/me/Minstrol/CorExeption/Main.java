package me.Minstrol.CorExeption;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import me.Minstrol.CorExeption.Listeners.*;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Main extends JavaPlugin implements Listener, PluginMessageListener {
    public NPC islandWars;
    public NPC stranded;
    public NPC NpcMinstrol;
    public NPC NpcKing_of_norway;
    Connection connection;

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getPluginManager().registerEvents(new ArrowHit(this), this);
        Bukkit.getPluginManager().registerEvents(new Chat(this), this);
        Bukkit.getPluginManager().registerEvents(new Interaction(this), this);
        Bukkit.getPluginManager().registerEvents(new InvClick(this), this);
        Bukkit.getPluginManager().registerEvents(new Join(this), this);
        Bukkit.getPluginManager().registerEvents(new misc(), this);
        Bukkit.getPluginManager().registerEvents(new Move(), this);
        Bukkit.getPluginManager().registerEvents(new TabJoin(), this);
        Bukkit.getPluginManager().registerEvents(new Quit(this), this);
        Bukkit.getPluginManager().registerEvents(new NpcClick(this), this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
        loadConfig();
        Server server = Bukkit.getServer();
        ConsoleCommandSender console = server.getConsoleSender();
        console.sendMessage(ChatColor.YELLOW + "Config has been loaded!");
        NPCRegistry registry = CitizensAPI.getNPCRegistry();
        islandWars = registry.createNPC(EntityType.SKELETON, ChatColor.YELLOW + "" + ChatColor.BOLD + "Island Wars");
        islandWars.spawn(new Location(Bukkit.getWorld("world"), 271.5, 71, 90.5, 68, 0));
        stranded = registry.createNPC(EntityType.SKELETON, ChatColor.YELLOW + "" + ChatColor.BOLD + "Stranded");
        stranded.spawn(new Location(Bukkit.getWorld("world"), 271.5, 71, 98.5, 110, 0));
        NpcKing_of_norway = registry.createNPC(EntityType.SKELETON, ChatColor.GREEN + "[NPC] King_of_norway");
        NpcKing_of_norway.spawn(new Location(Bukkit.getWorld("world"), -17.5, 69, 228.5, -130, 0));
        NpcMinstrol = registry.createNPC(EntityType.SKELETON, ChatColor.GREEN + "[NPC] Minstrol");
        NpcMinstrol.spawn(new Location(Bukkit.getWorld("world"), -36.5, 69, -115.5, -34, 0));
    }

    @Override
    public void onDisable() {
        islandWars.destroy();
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();
    }

    public void loadConfig(){
        saveConfig();
        saveDefaultConfig();
    }

    @EventHandler
    public void leave(PlayerJoinEvent event){
        Player player = event.getPlayer();
        String path = player.getName();
        getConfig().set(path, null);
        saveConfig();
    }

    public synchronized void openConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost/minecraft", "root", "password");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onplayerlogin(final PlayerLoginEvent event) {
        Player player = event.getPlayer();
        openConnection();
        try {
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement sqlcoins = connection.prepareStatement("SELECT coins FROM `player_data` WHERE Name=?;");
                sqlcoins.setString(1, player.getName());
                ResultSet resultcoins = sqlcoins.executeQuery();
                resultcoins.next();
                int coinsAmount = resultcoins.getInt("coins");
                getConfig().set("Users." + player.getName() + ".coins", coinsAmount);
                saveConfig();
                sqlcoins.close();
                resultcoins.close();
                PreparedStatement sqllevel = connection.prepareStatement("SELECT level FROM `player_data` WHERE Name=?;");
                sqllevel.setString(1, player.getName());
                ResultSet resultlevel = sqllevel.executeQuery();
                resultlevel.next();
                int levelAmount = resultlevel.getInt("level");
                getConfig().set("Users." + player.getName() + ".level", levelAmount);
                saveConfig();
                sqllevel.close();
                resultlevel.close();
                PreparedStatement sqlchatNot = connection.prepareStatement("SELECT chatNot FROM `player_data` WHERE Name=?;");
                sqlchatNot.setString(1, player.getName());
                ResultSet resultchatNot = sqlchatNot.executeQuery();
                resultchatNot.next();
                Boolean chatNotBool = resultchatNot.getBoolean("chatNot");
                getConfig().set("Users." + player.getName() + ".settings.chatNot", chatNotBool);
                saveConfig();
                sqlchatNot.close();
                resultchatNot.close();
                PreparedStatement sqlflying = connection.prepareStatement("SELECT flying FROM `player_data` WHERE Name=?;");
                sqlflying.setString(1, player.getName());
                ResultSet resultflying = sqlflying.executeQuery();
                resultflying.next();
                Boolean flyingBool = resultflying.getBoolean("flying");
                getConfig().set("Users." + player.getName() + ".flying", flyingBool);
                saveConfig();
                sqlflying.close();
                resultflying.close();
                Server server = Bukkit.getServer();
                ConsoleCommandSender console = server.getConsoleSender();
                console.sendMessage(ChatColor.YELLOW + player.getName() + "'s " + ChatColor.GOLD + "cache has been loaded!");
            } else {
                PreparedStatement newPlayer;
                String prepared = "INSERT INTO `player_data` VALUES(?,?,1,250,1,1,1,0,100,3,0,0,0);";
                newPlayer = connection.prepareStatement(prepared);
                newPlayer.setString(1, player.getUniqueId().toString());
                newPlayer.setString(2, player.getName());
                newPlayer.executeUpdate();
               cacheData(player);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public void cacheData(Player player) {
        openConnection();
        try {
            PreparedStatement sqlcoins = connection.prepareStatement("SELECT coins FROM `player_data` WHERE Name=?;");
            sqlcoins.setString(1, player.getName());
            ResultSet resultcoins = sqlcoins.executeQuery();
            resultcoins.next();
            int coinsAmount = resultcoins.getInt("coins");
            getConfig().set("Users." + player.getName() + ".coins", coinsAmount);
            saveConfig();
            sqlcoins.close();
            resultcoins.close();
            PreparedStatement sqllevel = connection.prepareStatement("SELECT level FROM `player_data` WHERE Name=?;");
            sqllevel.setString(1, player.getName());
            ResultSet resultlevel = sqllevel.executeQuery();
            resultlevel.next();
            int levelAmount = resultlevel.getInt("level");
            getConfig().set("Users." + player.getName() + ".level", levelAmount);
            saveConfig();
            sqllevel.close();
            resultlevel.close();
            PreparedStatement sqlchatNot = connection.prepareStatement("SELECT chatNot FROM `player_data` WHERE Name=?;");
            sqlchatNot.setString(1, player.getName());
            ResultSet resultchatNot = sqlchatNot.executeQuery();
            resultchatNot.next();
            Boolean chatNotBool = resultchatNot.getBoolean("chatNot");
            getConfig().set("Users." + player.getName() + ".settings.chatNot", chatNotBool);
            saveConfig();
            sqlchatNot.close();
            resultchatNot.close();
            PreparedStatement sqlflying = connection.prepareStatement("SELECT flying FROM `player_data` WHERE Name=?;");
            sqlflying.setString(1, player.getName());
            ResultSet resultflying = sqlflying.executeQuery();
            resultflying.next();
            Boolean flyingBool = resultflying.getBoolean("flying");
            getConfig().set("Users." + player.getName() + ".flying", flyingBool);
            saveConfig();
            sqlflying.close();
            resultflying.close();
            Server server = Bukkit.getServer();
            ConsoleCommandSender console = server.getConsoleSender();
            console.sendMessage(ChatColor.YELLOW + player.getName() + "'s " + ChatColor.GOLD + "cache has been loaded!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }




    /**DATABASE STUFF**/

    public boolean playerDataContainsPlayer(Player player) { //gets the player in the database
        Boolean containsThePlayer = false;
        if (playerDataContainsPlayerUUID(player)) {
            try {
                PreparedStatement sql = connection.prepareStatement("SELECT Name FROM `player_data` WHERE UUID=?;");
                sql.setString(1, player.getUniqueId().toString());
                ResultSet result = sql.executeQuery();
                result.next();
                String bool = result.getString("Name");
                sql.close();
                result.close();
                if (bool.equals(player.getName())) {
                    containsThePlayer = true;
                } else {
                    player.kickPlayer("Name change detected! Please relog.");
                    try {
                        PreparedStatement sql2 = connection.prepareStatement("SELECT Name FROM `player_data` WHERE UUID=?;");
                        sql2.setString(1, player.getUniqueId().toString());
                        sql2.close();
                        result.close();
                        PreparedStatement sql3 = connection.prepareStatement("SELECT Name FROM `player_data` WHERE Name=?;");
                        sql3.setString(1, player.getName());
                        sql3.close();
                        result.close();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                containsThePlayer = false;
            }
        } else {
            containsThePlayer = false;
        }
        return containsThePlayer;
    }

    public boolean playerDataContainsPlayerUUID(Player player) { //gets the player in the database
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM `player_data` WHERE UUID=?;");
            sql.setString(1, player.getUniqueId().toString());
            ResultSet resultSet = sql.executeQuery();
            boolean containsPlayersUUID = resultSet.next();

            sql.close();
            resultSet.close();

            return containsPlayersUUID;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public int getDatabaseInt(String table, String select, Player player) {
        int bool = 0;
        openConnection();
        try {
            if (playerDataContainsPlayer(player)) {
                PreparedStatement sql = connection.prepareStatement("SELECT " + select + " FROM `" + table + "` WHERE Name=?;");
                sql.setString(1, player.getName());
                ResultSet result = sql.executeQuery();
                result.next();
                bool = result.getInt(select);
                sql.close();
                result.close();
            }
        } catch (Exception e) {
        } finally {
            closeConnection();
        }
        return bool;
    }

    public boolean getDatabaseBoolean(String table, String select, Player player) {
        boolean bool = false;
        try {
            if (playerDataContainsPlayer(player)) {
                PreparedStatement sql = connection.prepareStatement("SELECT " + select +" FROM `" + table + "` WHERE Name=?;");
                sql.setString(1, player.getName());
                ResultSet result = sql.executeQuery();
                result.next();
                bool = result.getBoolean(select);
                sql.close();
                result.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return bool;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        PermissionUser pUser = PermissionsEx.getUser((Player) sender);
        final Player player = (Player) sender;
        if (command.getName().equalsIgnoreCase("fly")){
            if (pUser.inGroup("DEFAULT")){
                player.sendMessage(ChatColor.RED + "You cannot do this!");
            } else {
                if (getConfig().getBoolean("Users." + player.getName() + ".flying")) {
                    player.setAllowFlight(false);
                    getConfig().set("Users." + player.getName() + ".flying", Boolean.valueOf(false));
                    player.sendMessage(ChatColor.BLUE + "Flying has been disabled!");
                } else {
                    player.setAllowFlight(true);
                    getConfig().set("Users." + player.getName() + ".flying", Boolean.valueOf(true));
                    player.sendMessage(ChatColor.BLUE + "Flying has been enabled!");
                }
            }
        }
        if (command.getName().equalsIgnoreCase("gm")) {
            if (pUser.inGroup("ADMIN") || pUser.inGroup("OWNER")) {
                if (player.getGameMode().equals(GameMode.ADVENTURE)) {
                    player.setGameMode(GameMode.CREATIVE);
                } else {
                    player.setGameMode(GameMode.ADVENTURE);
                }
            } else {
                player.sendMessage(ChatColor.RED + "You cannot do this!");
            }
        }
        if (command.getName().equalsIgnoreCase("info")){
            player.sendMessage(ChatColor.YELLOW + "Version: " + ChatColor.GOLD +  "1.1");
            player.sendMessage(ChatColor.GOLD + "Wow, well hello there... The reason this command has been added is to give you a brief description of the core plugin 'CorExemption', this plugin is the most is one of the most advanced core's that we have ever had handling stuff from the moment you join to the moment you leave, the core handles most of the features on this server!");
        }
        if (command.getName().equalsIgnoreCase("ts")){
            player.sendMessage(ChatColor.GREEN + "Ts address: ts.minecation.net");
        }
        if (command.getName().equalsIgnoreCase("store")){
            player.sendMessage(ChatColor.GREEN + "Store link: http://store.minecation.net");
        }
        if (command.getName().equalsIgnoreCase("stuck")){
            player.teleport(new Location(Bukkit.getWorld("world"), 996.5, 78, 1380.5, -90, 0));
            player.sendMessage(ChatColor.GREEN + "Try not to get stuck again, alright?");
        }
        if (command.getName().equalsIgnoreCase("vote")){
            player.sendMessage("Coming soon...");
        }
        return false;
    }
}
