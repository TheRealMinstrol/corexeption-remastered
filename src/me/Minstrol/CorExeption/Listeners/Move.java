package me.Minstrol.CorExeption.Listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Random;

public class Move implements Listener {

    @EventHandler
    public void moving(PlayerMoveEvent event) {
        final Player p = event.getPlayer();
        if (p.getLocation().getBlock().getType().equals(Material.STONE_PLATE)) {
            Location launch = p.getLocation().clone();
            launch.add(0, 10, 0);
            launch.setPitch(-10);
            p.setVelocity(launch.getDirection().multiply(2));
            p.playSound(p.getLocation(), Sound.ENTITY_CAT_PURREOW, 10, 5);
        }
        if (p.getLocation().getBlock().getType().equals(Material.GOLD_PLATE)) {
            Location launch = p.getLocation().clone();
            launch.add(0, 0, 0);
            launch.setPitch(-15);
            p.setVelocity(launch.getDirection().multiply(5));
            p.playSound(p.getLocation(), Sound.ENTITY_CAT_PURREOW, 10, 5);
        }
        if (p.getLocation().add(0, -2, 0).getBlock().getType().equals(Material.SPONGE)) {
            Location launch = p.getLocation().clone();
            launch.add(0, 0, 0);
            launch.setPitch(-90);
            Random rn = new Random();
            int randomint = rn.nextInt(4) + 1;
            p.setVelocity(launch.getDirection().multiply(randomint));
        }
    }
}
