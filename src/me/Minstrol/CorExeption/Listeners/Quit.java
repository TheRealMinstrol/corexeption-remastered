package me.Minstrol.CorExeption.Listeners;

import me.Minstrol.CorExeption.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Quit implements Listener {
    Connection connection;
    Main plugin;

    public Quit(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void leave(PlayerQuitEvent event){
        Player player = event.getPlayer();
        updateDatabaseBoolean("player_data", "chatNot", player, plugin.getConfig().getBoolean("Users." + player.getName() + ".settings.chatNot"));
    }

    public void updateDatabaseBoolean(String table, String select, Player player, boolean bool){
        openConnection();
        try {
            if (playerDataContainsPlayer(player)) {
                PreparedStatement sql = connection.prepareStatement("SELECT " + select + " FROM `" + table + "` WHERE Name=?;");
                sql.setString(1, player.getName());
                PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `" + table + "` SET " + select + "=? WHERE Name=?");
                loginsupdate.setBoolean(1, bool);
                loginsupdate.setString(2, player.getName());
                loginsupdate.executeUpdate();
                loginsupdate.close();
                sql.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean playerDataContainsPlayer(Player player) { //gets the player in the database
        openConnection();
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM `player_data` WHERE Name=?;");
            sql.setString(1, player.getName());
            ResultSet resultSet = sql.executeQuery();
            boolean containsPlayer = resultSet.next();

            sql.close();
            resultSet.close();

            return containsPlayer;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized void openConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost/minecraft", "root", "password");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
