package me.Minstrol.CorExeption.Listeners;

import me.Minstrol.CorExeption.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Chat implements Listener {
    Main plugin;
    ArrayList<String> blocked = new ArrayList<>();

    public Chat(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void playerChat(AsyncPlayerChatEvent event){
        Player player = event.getPlayer();
        PermissionUser pUser = PermissionsEx.getUser(player);
        FileConfiguration swearingConfig = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "swearing.yml"));
        event.setCancelled(true);
        List<String> blockedWords = swearingConfig.getStringList("words");
        for (String s : blockedWords){
            if (event.getMessage().toLowerCase().contains(s)) {
                if (!(pUser.inGroup("ADMIN") || pUser.inGroup("OWNER"))) {
                    if (!blocked.contains(player.getName())) {
                        blocked.add(player.getName());
                    }
                }
            }
        }
        if (blocked.contains(player.getName())){
            player.sendMessage(ChatColor.RED + "Ohh, that language is not allowed on this server!");
            for (Player online : Bukkit.getOnlinePlayers()){
                PermissionUser pOnline = PermissionsEx.getUser(online);
                if (pOnline.inGroup("BUILD-TEAM") || pOnline.inGroup("HELPER") || pOnline.inGroup("MOD") || pOnline.inGroup("ADMIN") || pOnline.inGroup("OWNER")) {
                    online.sendMessage(ChatColor.RED + "[Blocked] " + ChatColor.WHITE + pUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + event.getMessage());
                    online.playSound(player.getLocation(), Sound.BLOCK_ANVIL_BREAK, 10, 5);
                }
            }
            blocked.remove(player.getName());
        } else {
            allowMessage(event.getMessage(), player);
        }

    }

    public void allowMessage(String message, Player player){
        PermissionUser pUser = PermissionsEx.getUser(player);
        for (Player online : Bukkit.getOnlinePlayers()){
            if (plugin.getConfig().getBoolean("Users." + online.getName() + ".settings.chatNot")) {
                if (message.contains(online.getName())) {
                    if (!pUser.inGroup("DEFAULT")) {
                        online.sendMessage(pUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + message.replaceAll(online.getName(), ChatColor.YELLOW + online.getName() + ChatColor.WHITE));
                        player.playSound(online.getLocation(), Sound.ENTITY_ITEM_PICKUP, 10, 5);
                    } else {
                        online.sendMessage(pUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + message.replaceAll(online.getName(), ChatColor.YELLOW + online.getName() + ChatColor.DARK_GRAY));
                        player.playSound(online.getLocation(), Sound.ENTITY_ITEM_PICKUP, 10, 5);
                    }
                } else {
                    if (!pUser.inGroup("DEFAULT")) {
                        online.sendMessage(pUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + message);
                    } else {
                        online.sendMessage(pUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + message);
                    }
                }
            } else {
                if (!pUser.inGroup("DEFAULT")) {
                    online.sendMessage(pUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + message);
                } else {
                    online.sendMessage(pUser.getPrefix() + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + message);
                }
            }
        }
        if (blocked.contains(player.getName())){
            blocked.remove(player.getName());
        }
    }
}
