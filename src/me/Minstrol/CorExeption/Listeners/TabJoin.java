package me.Minstrol.CorExeption.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class TabJoin implements Listener {

    @EventHandler
    public void playerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        PermissionUser permissionUser = PermissionsEx.getUser(player);
        try
        {
            Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
            Team color = board.getTeam(player.getName());
            if (color == null) {
                color = board.registerNewTeam(player.getName());
            }
            color.setPrefix(permissionUser.getPrefix());
            color.addPlayer(player);
            player.setScoreboard(board);
        }
        catch (NullPointerException localNullPointerException) {
            localNullPointerException.printStackTrace();
        }
    }
}
