package me.Minstrol.CorExeption.Listeners;

import me.Minstrol.CorExeption.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class ArrowHit implements Listener {

    Main plugin;

    public ArrowHit(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void onProjectileHitEvent(ProjectileHitEvent event) {
        if (!(event.getEntity().getShooter() instanceof Player)) return;
        if (event.getEntityType() == EntityType.ARROW) {
            Player player = (Player) event.getEntity().getShooter();
            final Projectile proj = event.getEntity();
            player.teleport(new Location(Bukkit.getWorld(proj.getWorld().getName()), proj.getLocation().getX(), proj.getLocation().getY(), proj.getLocation().getZ(), proj.getLocation().getPitch(), 0));
            player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 5, 5);
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                @Override
                public void run() {
                    proj.remove();
                }
            }, 40L);
        }
    }
}
