package me.Minstrol.CorExeption.Listeners;

import me.Minstrol.CorExeption.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class InvClick implements Listener {
    Inventory settings;
    Main plugin;
    ArrayList<String> cooldown = new ArrayList<>();

    public InvClick(Main instance) {
        plugin = instance;
    }

    public void openSettingsInv(Player player) {
        settings = Bukkit.createInventory(player, 36, ChatColor.DARK_GRAY + "Settings");
        createItem(settings, 12, Material.BANNER, 0, ChatColor.GOLD + "Chat notifications", new String[]{ChatColor.GRAY + "If a player mentions your", ChatColor.GRAY + "players name it will give", ChatColor.GRAY + "you a ding!"});
        createItem(settings, 14, Material.POTION, 8238, ChatColor.GOLD + "Players visibility", new String[]{ChatColor.GRAY + "When enabled you can see", ChatColor.GRAY + "all other players in your lobby!"});
        if (plugin.getConfig().getBoolean("Users." + player.getName() + ".settings.chatNot")) {
            createItem(settings, 21, Material.STAINED_CLAY, 13, ChatColor.GREEN + "Enabled", new String[]{ChatColor.GRAY + "Click to disable!"});
        } else {
            createItem(settings, 21, Material.STAINED_CLAY, 14, ChatColor.RED + "Disabled", new String[]{ChatColor.GRAY + "Click to enable!"});
        }
        createItem(settings, 23, Material.STAINED_CLAY, 13, ChatColor.GREEN + "Enabled", new String[]{ChatColor.GRAY + "Click to disable!"});
        createItem(settings, 36 - 1, Material.ARROW, 0, ChatColor.RED + "Exit", new String[]{});
        player.openInventory(settings);
    }

    public void createItem(Inventory inv, int slot, Material item, int dataint, String displayName, String[] lores) {
        ItemStack i = new ItemStack(item, 1, (byte) dataint);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(displayName);
        m.setLore(Arrays.asList(lores));
        i.setItemMeta(m);
        inv.setItem(slot, i);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) { //checks for the player to click an item in the GUI created
        final Player player = (Player) e.getWhoClicked();
        if (e.getCurrentItem() == null) return;
        if (e.getCurrentItem().getType().equals(Material.AIR)) return;
        e.setCancelled(true);

        /** Profile Inventory **/
        if (e.getCurrentItem().getType().equals(Material.PAPER)) {
            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Statistics")) {
                player.sendMessage(ChatColor.RED + "Coming soon...");
                Bukkit.getWorld(e.getWhoClicked().getWorld().getName()).playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 5);
                e.setCancelled(true);
            }
        }
        if (e.getCurrentItem().getType().equals(Material.REDSTONE_COMPARATOR)) {
            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Settings")) {
                openSettingsInv(player);
            }
        }

        if (e.getCurrentItem().getType().equals(Material.ARROW)) {
            player.closeInventory();
        }

        /** Settings Inventory **/
        if (e.getSlot() == 21 || e.getSlot() == 23) {
            if (!cooldown.contains(player.getName())) {
                if (e.getSlot() == 21) {
                    if (e.getCurrentItem().getType().equals(Material.STAINED_CLAY)) {
                        if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Enabled")) {
                            plugin.getConfig().set("Users." + player.getName() + ".settings.chatNot", false);
                            plugin.saveConfig();
                            createItem(settings, 21, Material.STAINED_CLAY, 14, ChatColor.RED + "Disabled", new String[]{ChatColor.GRAY + "Click to enable!"});
                            cooldown.add(player.getName());
                            player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 5);
                        } else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED + "Disabled")) {
                            plugin.getConfig().set("Users." + player.getName() + ".settings.chatNot", true);
                            plugin.saveConfig();
                            createItem(settings, 21, Material.STAINED_CLAY, 13, ChatColor.GREEN + "Enabled", new String[]{ChatColor.GRAY + "Click to disable!"});
                            cooldown.add(player.getName());
                            player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 5);
                        }
                    }
                }
                if (e.getSlot() == 23) {
                    player.sendMessage(ChatColor.RED + "This is a little buggy right now, this should become available in the next major patch!");
                    /**if (e.getCurrentItem().getType().equals(Material.STAINED_CLAY)) {
                     if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Enabled")) {
                     createItem(settings, 23, Material.STAINED_CLAY, 14, ChatColor.RED + "Disabled", new String[]{ChatColor.GRAY + "Click to enable!"});
                     player.updateInventory();
                     cooldown.add(player.getName());
                     plugin.getConfig().set("Users." + player.getName() + ".settings.playersVis", Boolean.valueOf(false));
                     Bukkit.getWorld(e.getWhoClicked().getWorld().getName()).playSound(player.getLocation(), Sound.CLICK, 10, 5);
                     unVanishLobby(player);
                     plugin.saveConfig();
                     } else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED + "Disabled")) {
                     createItem(settings, 23, Material.STAINED_CLAY, 13, ChatColor.GREEN + "Enabled", new String[]{ChatColor.GRAY + "Click to disable!"});
                     player.updateInventory();
                     cooldown.add(player.getName());
                     plugin.getConfig().set("Users." + player.getName() + ".settings.playersVis", Boolean.valueOf(true));
                     Bukkit.getWorld(e.getWhoClicked().getWorld().getName()).playSound(player.getLocation(), Sound.CLICK, 10, 5);
                     vanishLobby(player);
                     plugin.saveConfig();
                     }
                     }**/
                }
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        cooldown.remove(player.getName());
                    }
                }, 10L);
            } else {
                player.sendMessage(ChatColor.RED + "Calm down with the clicking!");
            }
        }

        /** Server selector **/
        if (e.getCurrentItem().getType().equals(Material.TNT)){
            teleportServer(player, "game_chosen");
        }
        if (e.getCurrentItem().getType().equals(Material.BOW)){
            player.sendMessage(ChatColor.RED + "Currently being developed!");
        }
        if (e.getCurrentItem().getType().equals(Material.WHEAT)){
            player.sendMessage(ChatColor.RED + "Currently being developed!");
        }
    }

    public void teleportServer(Player p, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch (IOException eee) {
            eee.printStackTrace();
        }
        p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
    }
}
