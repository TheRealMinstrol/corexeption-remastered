package me.Minstrol.CorExeption.Listeners;

import me.Minstrol.CorExeption.Main;
import net.minecraft.server.v1_9_R1.IChatBaseComponent;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.Arrays;

public class Join implements Listener {
    Main plugin;

    public Join(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        player.setGameMode(GameMode.ADVENTURE);
        player.teleport(new Location(Bukkit.getWorld("world"), 256.5, 71, 94.5, 90, 0));
        player.getInventory().clear();
        createItem(player.getInventory(), 4, Material.COMPASS, 0, ChatColor.GREEN + "Players Games" + ChatColor.GRAY + " (Right Click)", new String[]{ChatColor.DARK_GRAY + "Click to view our games!"});
        createItem(player.getInventory(), 9, Material.ARROW, 0, "", new String[]{});
        givePlayerProfile(player);
        givePlayerBow(player);
        player.getInventory().setHeldItemSlot(4);
        player.setAllowFlight(plugin.getConfig().getBoolean("Users." + player.getName() + ".flying"));
        player.setExp((float) plugin.getConfig().getInt("Users." + player.getName() + ".level"));
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                sendTitlePackets(player);
            }
        }, 20L);
        player.sendMessage(ChatColor.GREEN + "Welcome to the Minecation hub!");
        player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 5, 5);
        PermissionUser pUser = PermissionsEx.getUser(player);
        if (pUser.inGroup("DEFAULT")){
            e.setJoinMessage(null);
        } else {
            e.setJoinMessage(pUser.getPrefix() + player.getName() + ChatColor.YELLOW + " has joined the lobby!");
        }
    }

    public void sendTitlePackets(Player player){
        PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"Welcome to\",\"color\":\"yellow\",\"bold\":true}"), 40, 200, 20);
        PacketPlayOutTitle subtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"mc.minecation.net\",\"color\":\"green\"}"), 40, 200, 20);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(subtitle);
    }

    public void givePlayerProfile(Player player){
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(player.getName());
        meta.setDisplayName(ChatColor.GOLD + player.getName() + "'s Profile" + ChatColor.GRAY + " (Right Click)");
        skull.setItemMeta(meta);
        player.getInventory().setItem(6, skull);
    }

    public void givePlayerBow(Player player){
        ItemStack i = new ItemStack(Material.BOW, 1, (byte) 0);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(ChatColor.GREEN + "Telebow");
        m.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        m.addEnchant(Enchantment.DURABILITY, 1000, true);
        m.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        i.setItemMeta(m);
        player.getInventory().setItem(2, i);
    }

    public void createItem(Inventory inv, int slot, Material item, int dataint, String displayName, String[] lores) {
        ItemStack i = new ItemStack(item, 1, (byte) dataint);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(displayName);
        m.setLore(Arrays.asList(lores));
        i.setItemMeta(m);
        inv.setItem(slot, i);
    }

}
