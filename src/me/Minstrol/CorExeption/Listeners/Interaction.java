package me.Minstrol.CorExeption.Listeners;

import me.Minstrol.CorExeption.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.Arrays;

public class Interaction implements Listener {
    Main plugin;

    public Interaction(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (!e.getPlayer().isOp()) {
            if (!e.getAction().toString().toLowerCase().contains("right") || e.getItem() == null) return;
            if (e.getItem().getType().equals(Material.COMPASS)) {
                e.setCancelled(true);
                openSelectorInv(e.getPlayer());
            }
            if (e.getItem().getType().equals(Material.BOW)) {
                e.setCancelled(false);
            }
            if (e.getItem().getType().equals(Material.SAND)) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(ChatColor.RED + "Currently in development!");
            }
            if (e.getItem().getType().equals(Material.SKULL_ITEM)){
                openProfileInv(e.getPlayer());
            }
            if (e.getItem().getType().equals(Material.EMERALD)){
                e.getPlayer().sendMessage(ChatColor.RED + "Currently in development!");
                e.setCancelled(true);
            }
        }else {
            e.setCancelled(false);
        }
    }

    public void openSelectorInv(Player player){
        Inventory inv = Bukkit.createInventory(null, 9*3, ChatColor.DARK_GRAY + "Games");
        createItem(inv, 11, Material.TNT, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + "Chosen", new String[]{ChatColor.GRAY + "Will you be chosen?"});
        createItem(inv, 13, Material.BOW, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + "Reaction", new String[]{ChatColor.GRAY + "Test your reaction times!"});
        createItem(inv, 15, Material.WHEAT, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + "Barn Escape", new String[]{ChatColor.GRAY + "QUICK! The farmers coming!"});
        player.openInventory(inv);
    }

    public void openProfileInv(Player player){
        Inventory inv = Bukkit.createInventory(null, 27, ChatColor.DARK_GRAY + player.getName() + "'s Profile");
        createItem(inv, 12, Material.PAPER, 0, ChatColor.GOLD + "Statistics", new String[]{ChatColor.GRAY + "Click to view your statistics!"});
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(player.getName());
        meta.setDisplayName(ChatColor.GOLD + "Your Info");
        PermissionUser pUser = PermissionsEx.getUser(player);
        meta.setLore(Arrays.asList(ChatColor.GRAY + "Name: " + ChatColor.WHITE + player.getName(), ChatColor.GRAY + "Rank: " + pUser.getPrefix(), ChatColor.GRAY + "Coins: " + ChatColor.WHITE + plugin.getConfig().getInt("Users." + player.getName() + ".coins"), ChatColor.GRAY + "Level: " + ChatColor.WHITE +  plugin.getConfig().getInt("Users." + player.getName() + ".level")));
        skull.setItemMeta(meta);
        inv.setItem(13, skull);
        createItem(inv, 14, Material.REDSTONE_COMPARATOR, 0, ChatColor.GOLD + "Settings", new String[]{ChatColor.GRAY + "Click to open the settings", ChatColor.GRAY + "menu!"});
        createItem(inv, (9*3)-1, Material.ARROW, 0, ChatColor.RED + "Exit", new String[]{});
        player.openInventory(inv);
    }

    public void createItem(Inventory inv, int slot, Material item, int dataint, String displayName, String[] lores) {
        ItemStack i = new ItemStack(item, 1, (byte) dataint);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(displayName);
        m.setLore(Arrays.asList(lores));
        i.setItemMeta(m);
        inv.setItem(slot, i);
    }
}
