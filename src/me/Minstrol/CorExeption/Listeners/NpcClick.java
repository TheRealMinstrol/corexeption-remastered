package me.Minstrol.CorExeption.Listeners;

import me.Minstrol.CorExeption.Main;
import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;

public class NpcClick implements Listener {
    Main plugin;
    public NpcClick(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void onNPCclick(NPCRightClickEvent event){
        Player player = event.getClicker();
        if (event.getNPC().getStoredLocation().equals(plugin.NpcMinstrol.getStoredLocation())){
            player.sendMessage(ChatColor.GREEN + "[NPC] Minstrol" + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + "And the current time is! " + new Date().getTime());
        }
        if (event.getNPC().getStoredLocation().equals(plugin.NpcKing_of_norway.getStoredLocation())){
            player.sendMessage(ChatColor.GREEN + "[NPC] King_of_norway" + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + "F... Cough!");
        }
        if (event.getNPC().getStoredLocation().equals(plugin.islandWars.getStoredLocation())){
            teleportServer(player, "lobby_islandWars_1");
        }
        if (event.getNPC().getStoredLocation().equals(plugin.stranded.getStoredLocation())){
            teleportServer(player, "game_stranded");
        }
    }
    @EventHandler
    public void onNPCclick(NPCLeftClickEvent event){
        Player player = event.getClicker();
        if (event.getNPC().getStoredLocation().equals(plugin.NpcMinstrol.getStoredLocation())){
            player.sendMessage(ChatColor.GREEN + "[NPC] Minstrol" + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + "And the current time is! " + new Date().getTime());
        }
        if (event.getNPC().getStoredLocation().equals(plugin.NpcKing_of_norway.getStoredLocation())){
            player.sendMessage(ChatColor.GREEN + "[NPC] King_of_norway" + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + "F... Cough!");
        }
        if (event.getNPC().getStoredLocation().equals(plugin.islandWars.getStoredLocation())){
            teleportServer(player, "lobby_islandWars_1");
        }
        if (event.getNPC().getStoredLocation().equals(plugin.stranded.getStoredLocation())){
            teleportServer(player, "game_stranded");
        }
    }

    public void teleportServer(Player p, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch (IOException eee) {
            eee.printStackTrace();
        }
        p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
    }
}
